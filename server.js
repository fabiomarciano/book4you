var ExpressSrv = require('express');

var app = ExpressSrv();
app.use('/', ExpressSrv.static('htdocs'));

var server = app.listen(3000, function() {
	console.log('listen on http://localhost:%s', server.address().port);
});
