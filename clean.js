var FileSystem = require('fs');
var PathSystem = require('path');

var rmdir = function(dir) {
	var list = FileSystem.readdirSync(dir);

	for (var i = 0, j = list.length; i < j; i++) {
		var file = PathSystem.join(dir, list[i]);
		var stat = FileSystem.statSync(file);

		if (file != '.' && file != '..') {
			if (stat.isDirectory()) {
				rmdir(file);
				console.log('Removing directory: ', file);
			} else {
				FileSystem.unlinkSync(file);
				console.log('Removing file: ', file);
			}
		}
	}
	FileSystem.rmdirSync(dir);
}

if (FileSystem.existsSync('htdocs')) {
	console.log('Cleaning up htdocs directory');
	rmdir('htdocs');
}

if (!FileSystem.existsSync('htdocs')) {
	console.log('Creating htdocs directory');
	FileSystem.mkdirSync('htdocs', 0775);
	FileSystem.mkdirSync('htdocs/assets', 0775);
	FileSystem.mkdirSync('htdocs/assets/media', 0775);
	FileSystem.mkdirSync('htdocs/assets/style', 0775);
	FileSystem.mkdirSync('htdocs/assets/script', 0775);
}
