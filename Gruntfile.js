module.exports = function(grunt) {
	grunt.initConfig({
		copy: {
			main: {
				expand: true,
				cwd: 'src',
				src: '*.html',
				dest: 'htdocs/'
			},
			media: {
				expand: true,
				cwd: 'src/assets/media',
				src: '*.*',
				dest: 'htdocs/assets/media/'
			},
			script: {
				expand: true,
				cwd: 'src/assets/script',
				src: '*.*',
				dest: 'htdocs/assets/script/'
			}
		},
		less: {
			build: {
				src: ['src/assets/style/main.less', 'src/assets/style/header.less'],
				dest: 'htdocs/assets/style/main.css'
			}
		},
		includes: {
			build: {
				cwd: 'src',
				src: '*.html',
				dest: 'htdocs/',
				options: {
					flatten: true,
					includePath: 'src/include',
					banner: '<!-- File included using grunt -->\n'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-includes');

	grunt.registerTask('build', ['copy', 'includes', 'less']);
	grunt.registerTask('default', ['build']);
};
